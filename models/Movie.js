const mongoose = require("mongoose");
const Schema = mongoose.Schema;
//create schema
const MovieSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  poster_path: {
    type: String
  },
  release_date: {
    type: Date
  },
  cast: {
    type: Array
  },
  rating: {
    type: Number
  }
});

module.exports = Movie = mongoose.model("movies", MovieSchema);
