import { GET_ITEMS, RATE_ITEM } from "../actions/types";

const initialState = {
  items: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ITEMS:
      return {
        ...state,
        items: action.payload
      };
    case RATE_ITEM:
      return {
        ...state,
        items: state.items.map(item =>
          item._id === action.payload.id
            ? { ...item, rating: action.payload.rating }
            : item
        )
      };
    default:
      return state;
  }
}
