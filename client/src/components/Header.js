import React from "react";
import { Navbar, Nav, Button } from "react-bootstrap";

const Header = props => {
  return (
    <Navbar id="nav" bg="light" expand="lg">
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse className="basic-navbar-nav">
        <Nav>
          <Button
            className={props.activeTab === "movies" ? "active" : null}
            type="button"
            onClick={props.toggleActive}
          >
            Movies
          </Button>
          <Button
            className={props.activeTab === "shows" ? "active" : null}
            type="button"
            onClick={props.toggleActive}
          >
            Shows
          </Button>
        </Nav>
      </Navbar.Collapse>
      <Navbar.Collapse className="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link href="/login">Login</Nav.Link>
          <Nav.Link href="/register">Register</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};
export default Header;
