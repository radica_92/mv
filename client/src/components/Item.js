import React, { Component } from "react";
import Rater from "react-rater";
import "react-rater/lib/react-rater.css";
import { rateItem } from "../actions/movieActions";
import { connect } from "react-redux";
import "./Item.css";
import moment from "moment";

class Item extends Component {
  state = {
    prevRating: 0,
    data: []
  };

  handleRating = (id, { rating }) => {
    const prev = this.props.movies.items.filter(item => item._id === id);
    this.setState(
      {
        prevRating: prev[0].rating,
        [id]: [...this.state.data, rating]
      },
      function() {
        const newRating = (this.state.prevRating + rating) / 2;
        this.props.rateItem(id, newRating, this.props.activeTab);
      }
    );
  };
  render() {
    return (
      <div className="container">
        {this.props.movies.items.map(item => {
          return (
            <div>
              <h3 id="title">{item.title}</h3>
              <div className="row">
                <div id="left" className="col-lg-3 col-sm-4">
                  <img src={item.poster_path} />
                  <div className="rating">
                    Rating:{" "}
                    <Rater total={5} rating={item.rating} interactive={false} />
                  </div>
                </div>
                <div id="right" className="col-lg-9 col-sm-8">
                  <div>
                    Rate this:{" "}
                    <Rater
                      key={item._id}
                      total={5}
                      interactive={true}
                      rating={
                        !this.state[item._id] ? 0 : this.state[item._id][0]
                      }
                      onRate={this.handleRating.bind(this, item._id)}
                    />
                  </div>
                  <p className="text-muted">{item.description}</p>
                  <span>
                    Actors:{" "}
                    {item.cast.map(actor => (
                      <span>{actor} </span>
                    ))}
                  </span>
                  <div className="text-muted">
                    Released: {moment(item.release_date).format("DD/MM/YYYY")}
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  movies: state.movie
});

export default connect(
  mapStateToProps,
  { rateItem }
)(Item);
