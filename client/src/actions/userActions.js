import { REGISTER_USER, LOGIN } from "./types";
import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

export const registerUser = (userData, history) => dispatch => {
  axios.post("/api/users/register", userData).then(res => {
    history.push("/login");
    dispatch({
      type: REGISTER_USER,
      payload: res.data
    });
  });
};

// Login - Get User Token
export const loginUser = userData => dispatch => {
  axios.post("/api/users/login", userData).then(res => {
    // Save to localStorage
    const { token } = res.data;
    // Set token to ls
    localStorage.setItem("jwtToken", token);
    // Set token to Auth header
    setAuthToken(token);
    // Decode token to get user data
    const decoded = jwt_decode(token);
    // Set current user
    dispatch(setCurrentUser(decoded));
  });
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: LOGIN,
    payload: decoded
  };
};
