export const GET_ITEMS = "GET_ITEMS";

export const RATE_ITEM = "RATE_ITEM";

export const REGISTER_USER = "REGISTER_USER";

export const LOGIN = "LOGIN";
