import { GET_ITEMS, RATE_ITEM } from "./types";
import axios from "axios";

export const getItems = active => dispatch => {
  if (active === "movies") {
    axios.get("/api/movies").then(res => {
      dispatch({
        type: GET_ITEMS,
        payload: res.data
      });
    });
  } else {
    axios.get("/api/shows").then(res => {
      dispatch({
        type: GET_ITEMS,
        payload: res.data
      });
    });
  }
};

export const rateItem = (id, rating, active) => dispatch => {
  if (active === "movies") {
    console.log(rating);
    axios.post(`/api/movies/${id}`, { rating });
  } else {
    axios.post(`/api/shows/${id}`, { rating });
  }
  const payload = { id: id, rating: rating }; // tj <==>{id,rating}
  dispatch({
    type: RATE_ITEM,
    payload: payload
  });
};
