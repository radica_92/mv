const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");

const users = require("./routes/api/users");
const movies = require("./routes/api/movies");
const shows = require("./routes/api/shows");

const app = express();
//body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const db = require("./config/keys").mongoURI;

mongoose
  .connect(db)
  .then(() => {
    console.log("Mongo connected...");
  })
  .catch(err => console.log(err));

app.use(passport.initialize());

require("./config/passport")(passport);

app.use("/api/movies", movies);
app.use("/api/shows", shows);
app.use("/api/users", users);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server started on port ${port}`));
