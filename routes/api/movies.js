const express = require("express");
const router = express.Router();

const Movie = require("../../models/Movie");

//@route GET api/movies
//@desc get movies from db
//@access Public
router.get("/", (req, res) => {
  Movie.find()
    .sort({ rating: -1 })
    .then(movies => res.json(movies));
});

router.post("/:id", (req, res) => {
  Movie.findById(req.params.id.toString()).catch(err =>
    console.log("Error", err)
  );
  Movie.updateOne(
    { _id: req.params.id.toString() },
    { $set: { rating: req.body.rating } },
    function(err, raw) {
      if (err) {
        res.json(err);
      }
      res.json(raw);
    }
  );
});

module.exports = router;
