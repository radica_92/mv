const express = require("express");
const router = express.Router();

const Show = require("../../models/Show");

//@route GET api/shows
//@desc get shows from db
//@access Public
router.get("/", (req, res) => {
  Show.find()
    .sort({ rating: -1 })
    .then(shows => res.json(shows));
});

router.post("/:id", (req, res) => {
  Show.findById(req.params.id.toString()).catch(err =>
    console.log("Error", err)
  );
  Show.updateOne(
    { _id: req.params.id.toString() },
    { $set: { rating: req.body.rating } },
    function(err, raw) {
      if (err) {
        res.json(err);
      }
      res.json(raw);
    }
  );
});

module.exports = router;
